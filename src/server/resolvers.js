const fetch = require('node-fetch');
const { PubSub } = require('graphql-subscriptions');

const { USER_ADDED, USER_DELETED, USER_MODIFIED } = require('./constants');

const pubsub = new PubSub();
const API_URL = 'http://localhost:3001';

module.exports = {
  Query: {
    users: () =>
      fetch(`${API_URL}/users`)
        .then(rs => rs.json()),
    user: (parentValue, { id }) =>
      fetch(`${API_URL}/users/${id}`)
        .then(rs => rs.json())
  },
  Mutation: {
    addUser: async (parentValue, args) => {
      const promise = await fetch(`${API_URL}/users`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ ... args }),
      });
      const userAdded = await promise.json();
      pubsub.publish(USER_ADDED, {
        action: USER_ADDED,
        userAdded
      });
      return userAdded;
    },
    deleteUser: async (parentValue, { id }) => {
      const promise = await fetch(`${API_URL}/users/${id}`, {
        method: 'DELETE',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ id }),
      });
      const userDeleted = await promise.json();
      pubsub.publish(USER_DELETED, {
        userDeleted: { id }
      });
      // return userDeleted;
    },
    editUser: async (parentValue, args) => {
      const promise = await fetch(`${API_URL}/users/${args.id}`, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ ... args }),
      });
      const userModified = await promise.json();
      pubsub.publish(USER_MODIFIED, {
        action: USER_MODIFIED,
        userModified
      });
      return userModified;
    },
  },
  Subscription: {
    userAdded: {
      subscribe: () => pubsub.asyncIterator(USER_ADDED)
    },
    userModified: {
      subscribe: () => pubsub.asyncIterator(USER_MODIFIED)
    },
    userDeleted: {
      subscribe: () => pubsub.asyncIterator(USER_DELETED)
    }
  },
};
