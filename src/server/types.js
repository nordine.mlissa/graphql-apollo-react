module.exports = `
  type User {
    id: Int!,
    name: String,
    username: String,
    email: String,
    phone: String,
    website: String,
  }

  type Query {
    users: [User]
    user(id: Int!): User
  }

  type Mutation {
    addUser (
      name: String!,
      username: String!,
      email: String!,
      phone: String!,
      website: String!,
    ): User

    deleteUser (id: Int!): User

    editUser (
      id: Int!,
      name: String,
      username: String,
      email: String,
      phone: String,
      website: String,
    ): User
  }

  type Subscription {
    userAdded: User!
    userModified: User!
    userDeleted: User!
  }

  schema {
    query: Query
    mutation: Mutation
    subscription: Subscription
  }
`;
