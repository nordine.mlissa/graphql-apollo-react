import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { Link } from 'react-router-dom';

const deleteUser = gql`
  mutation deleteUser($id: Int!) {
    deleteUser(id: $id) {
      id
    }
  }
`;

export default class UserItem extends Component {
  render() {
    const { id, name, email } = this.props;
    return (
      <li>
        <Mutation mutation={deleteUser}>
          {(deleteUser, { data }) => (
            <button onClick={() => {
              deleteUser({ variables: { id } })
                .then(() => alert('Success !'))
                .catch(() => alert('An error occured...'))
            }}
            >
              DELETE
            </button>
          )}
        </Mutation>
        <Link to={`/modify/${id}`}><button>EDIT</button></Link>
        <Link to={`/user/${id}`}>{name} ({email})</Link>
      </li>
    );
  }
}
