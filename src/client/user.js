import React, { Component, Fragment } from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

const UserD = ({ data: { loading, error, user } }) => {

  if (loading) return 'loading...';
  if (error) return 'error !';

  return (
    <Fragment>
      {user && (
        <dl>
          <dt>Name</dt>
          <dd>{user.name}</dd>
          <dt>Username</dt>
          <dd>{user.username}</dd>
          <dt>Email</dt>
          <dd>{user.email}</dd>
          <dt>Phone</dt>
          <dd>{user.phone}</dd>
          <dt>Website</dt>
          <dd>{user.website}</dd>
        </dl>
      )}
    </Fragment>
  );
};

const USER_QUERY = gql`
  query user($id: Int!) {
    user(id: $id) {
      name,
      username,
      email,
      phone,
      website
    }
  }
`;

export default graphql(USER_QUERY, {
  options: ({ match }) => ({
    variables: { 
      id: match.params.id
    }
  })
})(UserD);
