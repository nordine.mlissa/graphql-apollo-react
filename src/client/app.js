import React from 'react';
import { Link, Switch, Route } from 'react-router-dom';

import UserList from './userlist';
import UserDetails from './user';
import Form from './form';
import Edit from './edit';

export default () => (
  <div>
    <ul>
      <li><Link to="/">Home</Link></li>
      <li><Link to="/create">Create user</Link></li>
    </ul>
    <hr />
    <UserList>
      <Switch>
        <Route path="/user/:id" component={UserDetails} />
        <Route path="/create" component={Form} />
        <Route path="/modify/:id" component={Edit} />
      </Switch>
    </UserList>
  </div>
);