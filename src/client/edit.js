import React, { Component, Fragment } from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import Form from './form';

const Edit = ({ data: { loading, error, user } }) => {

  if (loading) return 'loading...';
  if (error) return 'error !';

  return (
    <Form edit user={user} />
  );
};

const USER_QUERY = gql`
  query user($id: Int!) {
    user(id: $id) {
      id,
      name,
      username,
      email,
      phone,
      website
    }
  }
`;

export default graphql(USER_QUERY, {
  options: ({ match }) => ({
    variables: { 
      id: match.params.id
    }
  })
})(Edit);
