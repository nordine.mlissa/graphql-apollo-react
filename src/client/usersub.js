import React, { Component } from 'react';

import UserItem from './useritem';

export default class UserSub extends Component {
  componentDidMount() {
    this.props.subscribeToNewUsers();
    this.props.subscribeToModifiedUsers();
    this.props.subscribeToDeletedUsers();
  }

  render() {
    return (
      <ul>
        {
          this.props.users.map(u => (
            <UserItem key={u.id} {...u} />
          ))
        }
      </ul>
    );
  }
}