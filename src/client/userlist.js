import React, { Component } from 'react';
import { graphql, Subscription } from 'react-apollo';
import gql from 'graphql-tag';

import UserSub from './usersub';
import { USER_ADDED, USER_DELETED, USER_MODIFIED } from '../server/constants';

const USER_QUERY = gql`
  query {
    users {
      id,
      name,
      email
    }
  }
`;

const USER_ADDED_SUBSCRIPTION = gql`
  subscription userAdded {
    userAdded {
      id,
      name,
      email
    }
  }
`;

const USER_DELETED_SUBSCRIPTION = gql`
  subscription userDeleted {
    userDeleted {
      id
    }
  }
`;

const USER_MODIFIED_SUBSCRIPTION = gql`
  subscription userModified {
    userModified {
      id,
      name,
      username,
      email,
      phone,
      website
    }
  }
`;

const UserList = ({ children, data: { loading, error, users, subscribeToMore } }) => {
  if (loading) return 'loading...';
  if (error) return 'An error occured...';

  return (
    <div className="content">
      <div className="half">
        {
          users &&
          <UserSub
            users={users}
            subscribeToNewUsers={() =>
              subscribeToMore({
                document: USER_ADDED_SUBSCRIPTION,
                updateQuery: (prev, { subscriptionData }) => {
                  if (!subscriptionData.data) return prev;
                  return {
                    ...prev,
                    users: [
                      ...prev.users,
                      subscriptionData.data.userAdded
                    ],
                  };
                },
              })
            }
            subscribeToModifiedUsers={() =>
              subscribeToMore({
                document: USER_MODIFIED_SUBSCRIPTION,
                updateQuery: (prev, { subscriptionData }) => {
                  if (!subscriptionData.data) return prev;
                  return {
                    ...prev,
                    users: prev.users.map((u) => {
                      if (u.id === subscriptionData.data.userModified.id) {
                        return subscriptionData.data.userModified;
                      }
                      return u;
                    })
                  };
                }
              })
            }
            subscribeToDeletedUsers={() =>
              subscribeToMore({
                document: USER_DELETED_SUBSCRIPTION,
                updateQuery: (prev, { subscriptionData }) => {
                  if (!subscriptionData.data) return prev;
                  return {
                    ...prev,
                    users: prev.users.filter(u => u.id !== subscriptionData.data.userDeleted.id),
                  };
                }
              })
            }
          />
        }
      </div>
      <div className="half">{children}</div>
    </div>
  );
};

export default graphql(USER_QUERY)(UserList);
