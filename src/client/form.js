import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import gql from 'graphql-tag';

class Form extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    if (this.props.edit) {
      this.props.editUser({
        variables: {
          id: this.props.user.id,
          name: this.form.name.value,
          username: this.form.username.value,
          email: this.form.email.value,
          phone: this.form.phone.value,
          website: this.form.website.value,
        }
      }).then(() => {
        alert('Success !!');
        this.props.history.push('/');
      }).catch((error) => {
        alert('A problem occured..');
      });
    } else {
      this.props.addUser({
        variables: {
          name: this.form.name.value,
          username: this.form.username.value,
          email: this.form.email.value,
          phone: this.form.phone.value,
          website: this.form.website.value,
        }
      }).then(() => {
        alert('Success !!');
        this.props.history.push('/');
      }).catch((error) => {
        alert('A problem occured..');
      });
    }
  }

  render() {
    const { user: { name, username, email, phone, website } } = this.props;
    let f_name = name || '';
    let f_username = username || '';
    let f_email = email || '';
    let f_phone = phone || '';
    let f_website = name || '';

    return (
      <form ref={form => this.form = form} onSubmit={this.handleSubmit}>
        Name : <input name="name" defaultValue={f_name} required /><br />
        Username : <input name="username" defaultValue={f_username} required /><br />
        Email : <input name="email" defaultValue={f_email} required /><br />
        Phone : <input name="phone" defaultValue={f_phone} required /><br />
        Website : <input name="website" defaultValue={f_website}required /><br />
        <button type="submit">Valider</button>
      </form>
    )
  }
}

Form.defaultProps = {
  user: {
    id: -1,
    name: '',
    username: '',
    email: '',
    website: '',
    phone: '',
    edit: false,
  },
};

const addUser = gql`
  mutation addUser(
    $name: String!,
    $username: String!,
    $email: String!,
    $phone: String!,
    $website: String!,
  ) {
    addUser(
      name: $name
      username: $username
      email: $email
      phone: $phone
      website: $website
    ) {
      id,
      name
    }
  }
`;

const editUser = gql`
mutation editUser(
  $id: Int!
  $name: String!,
  $username: String!,
  $email: String!,
  $phone: String!,
  $website: String!,
) {
  editUser(
    id: $id
    name: $name
    username: $username
    email: $email
    phone: $phone
    website: $website
  ) {
    id,
    name
  }
}
`;

export default compose(
  graphql(addUser, { name: 'addUser' }),
  graphql(editUser, { name: 'editUser' }),
)(withRouter(Form));
