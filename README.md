# A GraphQL Backend Server with a React Client using Apollo

This project requires Node 8 or newer

## Installation
  ```
  $ git clone https://gitlab.com/nordine.mlissa/graphql-apollo-react

  $ cd graphql-apollo-react

  $ yarn
  ```

## Run the development server
  ```
  $ yarn dev
  ```
  --> React client app on http://localhost:3000

  --> GraphiQL on http://localhost:4000/graphql

  --> fake JSON API on http://localhost:3001
